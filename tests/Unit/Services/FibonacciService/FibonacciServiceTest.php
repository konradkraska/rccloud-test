<?php
declare(strict_types=1);

namespace App\Tests\Unit\Services\FibonacciService;

use App\Service\FibonacciService\Enums\FibonacciType;
use App\Service\FibonacciService\Factories\FibonacciMethodFactoryInterface;
use App\Service\FibonacciService\FibonacciService;
use App\Service\FibonacciService\Strategies\FibonacciMethodInterface;
use PHPUnit\Framework\TestCase;

class FibonacciServiceTest extends TestCase
{
    private FibonacciService $service;

    /**
     * @var (FibonacciMethodFactoryInterface&\Mockery\LegacyMockInterface)|(FibonacciMethodFactoryInterface&\Mockery\MockInterface)|\App\Service\FibonacciService\Factories\FibonacciMethodFactoryInterface|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    private $factory;

    /**
     * @var (FibonacciMethodInterface&\Mockery\LegacyMockInterface)|(FibonacciMethodInterface&\Mockery\MockInterface)|\App\Service\FibonacciService\Strategies\FibonacciMethodInterface|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    private $strategy;

    public function setUp(): void
    {
        parent::setUp();
        $this->factory = \Mockery::mock(FibonacciMethodFactoryInterface::class);
        $this->strategy = \Mockery::mock(FibonacciMethodInterface::class);
        $this->factory->shouldReceive('create')
            ->once()
            ->andReturn($this->strategy);

        $this->service = new FibonacciService($this->factory);
    }

    public function testSumMethod(): void
    {
        $this->strategy->shouldReceive('sum')
            ->once()
            ->andReturn(20);

        $result = $this->service->sum(FibonacciType::DAY);
        self::assertSame(20, $result);
    }

    public function testLastDigitMethod(): void
    {
        $this->strategy->shouldReceive('lastDigit')
            ->once()
            ->andReturn(30);

        $result = $this->service->lastValue(FibonacciType::DAY);
        self::assertSame(30, $result);
    }
}
