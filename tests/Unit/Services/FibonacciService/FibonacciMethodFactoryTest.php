<?php
declare(strict_types=1);

namespace App\Tests\Unit\Services\FibonacciService;

use App\Service\FibonacciService\Enums\FibonacciType;
use App\Service\FibonacciService\Factories\FibonacciMethodFactory;
use App\Service\FibonacciService\Strategies\FibonacciDayMethod;
use App\Service\FibonacciService\Strategies\FibonacciManualMethod;
use App\Service\FibonacciService\Strategies\FibonacciMonthMethod;
use PHPUnit\Framework\TestCase;

class FibonacciMethodFactoryTest extends TestCase
{
    private FibonacciMethodFactory $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->factory = new FibonacciMethodFactory();
    }

    public function testCreate(): void
    {
        self::assertInstanceOf(FibonacciDayMethod::class, $this->factory->create(FibonacciType::DAY));
        self::assertInstanceOf(FibonacciMonthMethod::class, $this->factory->create(FibonacciType::MONTH));
        self::assertInstanceOf(FibonacciManualMethod::class, $this->factory->create(FibonacciType::MANUAL));
    }
}
