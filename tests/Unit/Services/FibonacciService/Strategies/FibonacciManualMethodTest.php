<?php
declare(strict_types=1);

namespace App\Tests\Unit\Services\FibonacciService\Strategies;

use App\Service\FibonacciService\Strategies\FibonacciManualMethod;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Exception\ValidatorException;

class FibonacciManualMethodTest extends TestCase
{
    private FibonacciManualMethod $method;

    public function setUp(): void
    {
        parent::setUp();
        $this->method = new FibonacciManualMethod();
    }

    public function testPayloadValidationWithoutPayload(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Value must be present.');
        $this->method->sum();
    }

    public function testPayloadValidationNotNumeric(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Value must be numeric.');
        $this->method->sum([
            'value' => 'string',
        ]);
    }

    public function testPayloadValidationMinRange(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Value must be grater then 0.');
        $this->method->sum([
            'value' => -1,
        ]);
    }

    public function testPayloadValidationMaxRange(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage(sprintf('Value must be lest then %d.', FibonacciManualMethod::MAX_VALUE));
        $this->method->sum([
            'value' => FibonacciManualMethod::MAX_VALUE + 1,
        ]);
    }

    #[DataProvider('sumProvider')]
    public function testSum(int $value, int $expectedSum): void
    {
        $result = $this->method->sum([
            'value' => $value,
        ]);
        $this->assertSame($expectedSum, $result);
    }

    #[DataProvider('lastDigitProvider')]
    public function testLastDigit(int $value, int $expectedValue): void
    {
        $result = $this->method->lastDigit([
            'value' => $value,
        ]);
        $this->assertSame($expectedValue, $result);
    }

    public static function sumProvider(): array
    {
        return [
            [
                1,
                1,
            ],
            [
                2,
                2,
            ],
            [
                3,
                4,
            ],
            [
                39,
                165580140,
            ],
            [
                40,
                267914295,
            ],
            [
                41,
                433494436,
            ],
            [
                42,
                701408732,
            ],
            [
                43,
                1134903169,
            ],
            [
                44,
                1836311902,
            ],
        ];
    }

    public static function lastDigitProvider(): array
    {
        return [
            [
                1,
                1,
            ],
            [
                2,
                1,
            ],
            [
                3,
                2,
            ],
            [
                39,
                63245986,
            ],
            [
                40,
                102334155,
            ],
            [
                41,
                165580141,
            ],
            [
                42,
                267914296,
            ],
            [
                43,
                433494437,
            ],
            [
                44,
                701408733,
            ],
        ];
    }
}
