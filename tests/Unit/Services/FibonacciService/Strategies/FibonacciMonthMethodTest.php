<?php
declare(strict_types=1);

namespace App\Tests\Unit\Services\FibonacciService\Strategies;

use App\Service\FibonacciService\Strategies\FibonacciMonthMethod;
use Carbon\Carbon;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class FibonacciMonthMethodTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->method = new FibonacciMonthMethod();
    }

    #[DataProvider('sumProvider')]
    public function testSum(int $month, int $expectedSum): void
    {
        $testDate = Carbon::create(2023, $month, 1, 0, 0);
        Carbon::setTestNow($testDate);
        $result = $this->method->sum();
        $this->assertSame($expectedSum, $result);
    }

    #[DataProvider('lastDigitProvider')]
    public function testLastDigit(int $month, int $expectedValue): void
    {
        $testDate = Carbon::create(2023, $month, 1, 0, 0);
        Carbon::setTestNow($testDate);
        $result = $this->method->lastDigit();
        $this->assertSame($expectedValue, $result);
    }

    public static function sumProvider(): array
    {
        return [
            [
                1,
                1,
            ],
            [
                2,
                2,
            ],
            [
                3,
                4,
            ],
            [
                4,
                7,
            ],
            [
                5,
                12,
            ],
            [
                6,
                20,
            ],
            [
                7,
                33,
            ],
            [
                8,
                54,
            ],
            [
                9,
                88,
            ],
            [
                10,
                143,
            ],
            [
                11,
                232,
            ],
            [
                12,
                376,
            ],
        ];
    }

    public static function lastDigitProvider(): array
    {
        return [
            [
                1,
                1,
            ],
            [
                2,
                1,
            ],
            [
                3,
                2,
            ],
            [
                4,
                3,
            ],
            [
                5,
                5,
            ],
            [
                6,
                8,
            ],
            [
                7,
                13,
            ],
            [
                8,
                21,
            ],
            [
                9,
                34,
            ],
            [
                10,
                55,
            ],
            [
                11,
                89,
            ],
            [
                12,
                144,
            ],
        ];
    }
}
