<?php
declare(strict_types=1);

namespace App\Tests\Unit\Services\FibonacciService\Strategies;

use App\Service\FibonacciService\Strategies\FibonacciDayMethod;
use Carbon\Carbon;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class FibonacciDayMethodTest extends TestCase
{
    private FibonacciDayMethod $method;

    protected function setUp(): void
    {
        parent::setUp();

        $this->method = new FibonacciDayMethod();
    }

    #[DataProvider('sumProvider')]
    public function testSum(int $day, int $expectedSum): void
    {
        $testDate = Carbon::create(2023, 12, $day, 0, 0);
        Carbon::setTestNow($testDate);
        $result = $this->method->sum();
        $this->assertSame($expectedSum, $result);
    }

    #[DataProvider('lastDigitProvider')]
    public function testLastDigit(int $day, int $expectedValue): void
    {
        $testDate = Carbon::create(2023, 12, $day, 0, 0);
        Carbon::setTestNow($testDate);
        $result = $this->method->lastDigit();
        $this->assertSame($expectedValue, $result);
    }

    public static function sumProvider(): array
    {
        return [
            [
                1,
                1,
            ],
            [
                2,
                2,
            ],
            [
                3,
                4,
            ],
            [
                28,
                832039,
            ],
            [
                29,
                1346268,
            ],
            [
                30,
                2178308,
            ],
            [
                31,
                3524577,
            ],
        ];
    }

    public static function lastDigitProvider(): array
    {
        return [
            [
                1,
                1,
            ],
            [
                2,
                1,
            ],
            [
                3,
                2,
            ],
            [
                28,
                317811,
            ],
            [
                29,
                514229,
            ],
            [
                30,
                832040,
            ],
            [
                31,
                1346269,
            ],
        ];
    }
}
