<?php
declare(strict_types=1);

namespace App\Tests\Unit\Controllers;

use App\Kernel;
use App\Service\FibonacciService\Enums\FibonacciType;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testCalculateTypeDay(): void
    {
        $testDate = Carbon::create(2023, 12, 1, 0, 0);
        Carbon::setTestNow($testDate);

        $client = static::createClient();
        $client->jsonRequest('POST', '/api/calculate', [
            'type' => FibonacciType::DAY->value,
        ]);

        $response = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertSame(1, $response['sum']);
        $this->assertSame(1, $response['last_digit']);
    }

    public function testCalculateTypeMonth(): void
    {
        $testDate = Carbon::create(2023, 1, 1, 0, 0);
        Carbon::setTestNow($testDate);

        $client = static::createClient();
        $client->jsonRequest('POST', '/api/calculate', [
            'type' => FibonacciType::MONTH->value,
        ]);

        $response = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertSame(1, $response['sum']);
        $this->assertSame(1, $response['last_digit']);
    }

    public function testCalculateTypeManual(): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/calculate', [
            'type'    => FibonacciType::MANUAL->value,
            'payload' => [
                'value' => 1,
            ],
        ]);

        $response = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertSame(1, $response['sum']);
        $this->assertSame(1, $response['last_digit']);
    }

    public function testCalculateTypeInvalid(): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/calculate', [
            'type' => 'invalid',
        ]);

        $response = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertSame('The value you selected is not a valid choice.', $response['message']);
    }

    public function testCalculateTypeManualExpectError(): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/calculate', [
            'type' => FibonacciType::MANUAL->value,
        ]);

        $response = json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertSame('Value must be present.', $response['message']);
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }
}
