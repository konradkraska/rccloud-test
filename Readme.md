# RC Cloud test

## Installation

- composer install
- docker-compose up -d

## Useful commands

Inside container

- linter & static code analyse - `composer lint`
- unit tests - `composer test:unit`

## Usage

Website should be available under: http://127.0.0.1
