<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\FibonacciService\Enums\FibonacciType;
use App\Service\FibonacciService\FibonacciService;
use App\Service\FibonacciService\Requests\FibonacciRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    public function __construct(
        private readonly FibonacciService $fibonacciService,
    ) {
    }

    #[Route('api/calculate', methods: 'POST')]
    public function calculate(FibonacciRequest $request): JsonResponse
    {
        if (($response = $request->validate()) !== null) {
            return $response;
        }

        try {
            $data = [
                'sum'        => $this->fibonacciService->sum(FibonacciType::from($request->type), $request->payload),
                'last_digit' => $this->fibonacciService->lastValue(FibonacciType::from($request->type), $request->payload),
            ];
        } catch (\Throwable $exception) {
            return $this->handleError($exception);
        }

        return new JsonResponse($data);
    }

    private function handleError(\Throwable $throwable): JsonResponse
    {
        return new JsonResponse([
            'message' => $throwable->getMessage(),
        ], Response::HTTP_BAD_REQUEST);
    }
}
