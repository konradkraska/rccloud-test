<?php
declare(strict_types=1);

namespace App\Requests;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseAPIRequest
{
    public function __construct(
        protected ValidatorInterface $validator,
        protected RequestStack $requestStack,
    ) {
        $this->populate();
    }

    public function validate(): ?JsonResponse
    {
        $errors = $this->validator->validate($this);

        if (\count($errors) > 0) {
            $message = 'Something went wrong.';
            $error = $errors[0];

            if ($error instanceof ConstraintViolationInterface) {
                $message = $error->getMessage();
            }

            return new JsonResponse([
                'message' => $message,
            ]);
        }

        return null;
    }

    public function getRequest(): ?Request
    {
        return $this->requestStack->getCurrentRequest();
    }

    protected function populate(): void
    {
        if (null === $this->getRequest()) {
            throw new \RuntimeException('Request is not available.');
        }

        foreach ($this->getRequest()->toArray() as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }
}
