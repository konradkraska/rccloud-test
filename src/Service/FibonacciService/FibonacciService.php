<?php
declare(strict_types=1);

namespace App\Service\FibonacciService;

use App\Service\FibonacciService\Enums\FibonacciType;
use App\Service\FibonacciService\Factories\FibonacciMethodFactoryInterface;

class FibonacciService
{
    public function __construct(
        private FibonacciMethodFactoryInterface $factory,
    ) {
    }

    public function sum(FibonacciType $type, array $payload = []): int
    {
        $strategy = $this->factory->create($type);

        return $strategy->sum($payload);
    }

    public function lastValue(FibonacciType $type, array $payload = []): int
    {
        $strategy = $this->factory->create($type);

        return $strategy->lastDigit($payload);
    }
}
