<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Enums;

enum FibonacciType: string
{
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    case DAY = 'day';
    case MONTH = 'month';
    case MANUAL = 'manual';
}
