<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Strategies;

abstract class FibonacciMethod
{
    protected function fibonacci(int $n): array
    {
        $fib = [0, 1];

        for($i = 2; $i <= $n; ++$i) {
            $fib[$i] = $fib[$i - 1] + $fib[$i - 2];
        }

        return $fib;
    }
}
