<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Strategies;

use Symfony\Component\Validator\Exception\ValidatorException;

final class FibonacciManualMethod extends FibonacciMethod implements FibonacciMethodInterface
{
    public const MAX_VALUE = 44;

    public function sum(array $payload = []): int
    {
        $this->validatePayload($payload);
        $result = $this->fibonacci($payload['value']);

        return array_sum($result);
    }

    public function lastDigit(array $payload = []): int
    {
        $this->validatePayload($payload);
        $result = $this->fibonacci($payload['value']);

        return end($result);
    }

    private function validatePayload(array $payload): void
    {
        if (!isset($payload['value'])) {
            throw new ValidatorException('Value must be present.');
        }

        if (!is_numeric($payload['value'])) {
            throw new ValidatorException('Value must be numeric.');
        }

        if($payload['value'] > self::MAX_VALUE) {
            throw new ValidatorException(sprintf('Value must be lest then %d.', self::MAX_VALUE));
        }

        if($payload['value'] < 0) {
            throw new ValidatorException('Value must be grater then 0.');
        }
    }
}
