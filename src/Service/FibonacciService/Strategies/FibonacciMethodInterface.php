<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Strategies;

interface FibonacciMethodInterface
{
    public function sum(array $payload = []): int;

    public function lastDigit(array $payload = []): int;
}
