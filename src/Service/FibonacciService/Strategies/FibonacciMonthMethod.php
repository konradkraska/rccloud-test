<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Strategies;

use Carbon\Carbon;

final class FibonacciMonthMethod extends FibonacciMethod implements FibonacciMethodInterface
{
    public function sum(array $payload = []): int
    {
        $now = Carbon::now();

        $result = $this->fibonacci($now->month);

        return array_sum($result);
    }

    public function lastDigit(array $payload = []): int
    {
        $now = Carbon::now();
        $fib = $this->fibonacci($now->month);

        return end($fib);
    }
}
