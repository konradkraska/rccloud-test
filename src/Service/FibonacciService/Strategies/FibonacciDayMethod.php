<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Strategies;

use Carbon\Carbon;

final class FibonacciDayMethod extends FibonacciMethod implements FibonacciMethodInterface
{
    public function sum(array $payload = []): int
    {
        $now = Carbon::now();

        $result = $this->fibonacci($now->day);

        return array_sum($result);
    }

    public function lastDigit(array $payload = []): int
    {
        $now = Carbon::now();
        $fib = $this->fibonacci($now->day);

        return end($fib);
    }
}
