<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Requests;

use App\Requests\BaseAPIRequest;
use App\Service\FibonacciService\Enums\FibonacciType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotNull;

final class FibonacciRequest extends BaseAPIRequest
{
    #[NotNull]
    #[Choice(callback: [FibonacciType::class, 'values'])]
    public string $type;

    public array $payload = [];
}
