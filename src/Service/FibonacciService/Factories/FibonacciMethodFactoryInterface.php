<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Factories;

use App\Service\FibonacciService\Enums\FibonacciType;
use App\Service\FibonacciService\Strategies\FibonacciMethodInterface;

interface FibonacciMethodFactoryInterface
{
    public function create(FibonacciType $type): FibonacciMethodInterface;
}
