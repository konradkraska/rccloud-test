<?php
declare(strict_types=1);

namespace App\Service\FibonacciService\Factories;

use App\Service\FibonacciService\Enums\FibonacciType;
use App\Service\FibonacciService\Strategies\FibonacciDayMethod;
use App\Service\FibonacciService\Strategies\FibonacciManualMethod;
use App\Service\FibonacciService\Strategies\FibonacciMethodInterface;
use App\Service\FibonacciService\Strategies\FibonacciMonthMethod;

class FibonacciMethodFactory implements FibonacciMethodFactoryInterface
{
    public function create(FibonacciType $type): FibonacciMethodInterface
    {
        return match ($type) {
            FibonacciType::DAY    => new FibonacciDayMethod(),
            FibonacciType::MANUAL => new FibonacciManualMethod(),
            FibonacciType::MONTH  => new FibonacciMonthMethod(),
        };
    }
}
